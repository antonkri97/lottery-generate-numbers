package main

import (
	"math/rand"
	"net/http"
	"os"

	"github.com/antonkri97/lottery_nums/db"
	"github.com/antonkri97/lottery_nums/middlewares"
	"gopkg.in/gin-gonic/gin.v1"
	"gopkg.in/mgo.v2"
)

// PlaysDoc is a main document of plays collection
type PlaysDoc struct {
	Numbers   `bson:"numbers"`
	None      []PlayersResult `bson:"none"`
	TwoNums   []PlayersResult `bson:"twoNums"`
	ThreeNums []PlayersResult `bson:"threeNums"`
}

// Numbers is nested struct for random generate numbers
type Numbers struct {
	Fst int `bson:"fst"`
	Snd int `bson:"snd"`
	Thd int `bson:"thd"`
}

// PlayersResult is a nested struct in main document
type PlayersResult struct {
	Fst   int    `bson:"fst"`
	Snd   int    `bson:"snd"`
	Thd   int    `bson:"thd"`
	Email string `bson:"email"`
}

// Port is default port
const (
	Port = "8081"
)

func init() {
	db.Connect()
}

func main() {
	router := gin.New()

	router.Use(middlewares.Connect)
	router.Use(middlewares.ErrorHandler)

	router.POST("numbers", func(c *gin.Context) {
		mongo := c.MustGet("db").(*mgo.Database)

		document := PlaysDoc{
			Numbers: Numbers{
				Fst: rand.Intn(10),
				Snd: rand.Intn(10),
				Thd: rand.Intn(10),
			},
			None:      []PlayersResult{},
			TwoNums:   []PlayersResult{},
			ThreeNums: []PlayersResult{},
		}

		err := mongo.C("plays").Insert(&document)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Can't insert new document"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"status": "inserted"})
	})

	port := Port
	if len(os.Getenv("PORT")) > 0 {
		port = os.Getenv("PORT")
	}

	router.Run(":" + port)
}
