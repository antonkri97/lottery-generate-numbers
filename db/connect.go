package db

import (
	"fmt"

	"github.com/antonkri97/lottery_nums/utils"
	"gopkg.in/mgo.v2"
)

var (
	// Session stores mongo session
	Session *mgo.Session

	// Mongo stores the mongodb connection to the db
	Mongo *mgo.DialInfo
)

const (
	// MongoDBUrl is the default mongodb url that will be used to connect to the
	// database.
	MongoDBUrl = "mongodb://localhost:27017/lottery"
	UserNameDB = "lottery"
	PasswordDB = "0000"
)

// Connect connects to mongodb
func Connect() {
	/* uri := os.Getenv("MONGODB_URL")

	if len(uri) == 0 {
		uri = MongoDBUrl
	} */

	uri := utils.CheckEnv("MONGODB_URL", MongoDBUrl)

	mongo, err := mgo.ParseURL(uri)
	s, err := mgo.Dial(uri)
	if err != nil {
		fmt.Printf("Can't connect to mongo: %v\n", err)
		panic(err.Error())
	}

	usr := utils.CheckEnv("USERNAME_DB", UserNameDB)
	pwd := utils.CheckEnv("PASSWORD_DB", PasswordDB)

	s.DB("lottery").Login(usr, pwd)
	if err != nil {
		fmt.Printf("Can't login to mongo: %v\n", err)
		panic(err.Error())
	}

	s.SetSafe(&mgo.Safe{})
	fmt.Println("Connected to ", uri)
	Session = s
	Mongo = mongo
}
