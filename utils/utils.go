package utils

import (
	"errors"

	"os"

	"github.com/antonkri97/lottery_nums/models"
)

// PlayerResult определяет в какую группу в бд стоит записать игрока
// Возвращает:
// "threeNums" в случае джекпота;
// "twoNums", если игрок отгадал два числа;
// "none" в случае неудачи
func PlayerResult(player models.Plays, nmb models.NumsDB) (string, error) {
	result := 0

	if player.Fst == nmb.Numbers.Fst {
		result++
	}
	if player.Snd == nmb.Numbers.Snd {
		result++
	}
	if player.Thd == nmb.Numbers.Thd {
		result++
	}

	switch result {
	case 0, 1:
		return "none", nil
	case 2:
		return "twoNums", nil
	case 3:
		return "threeNums", nil
	default:
		return "", errors.New("can't get player's result")
	}
}

// CheckEnv возвращает значение по-умолчанию, если переменная окружения не опеределена
// либо саму переменную окружения
func CheckEnv(env string, def string) string {
	e := os.Getenv(env)
	if len(e) == 0 {
		return def
	}
	return e
}
