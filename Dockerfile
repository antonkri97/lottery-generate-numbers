FROM golang:latest

ADD . /go/src/github.com/antonkri97/lottery_nums

ENV GOBIN /go/bin

RUN go get gopkg.in/gin-gonic/gin.v1 && go get gopkg.in/mgo.v2 && go install /go/src/github.com/antonkri97/lottery_nums/main.go

EXPOSE 8081

CMD ["/go/bin/main"]
